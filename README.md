# Boxinator backend

The backend constructs the REST API that forms the core of this project and gives the front-end access to the PostgreSQL database. :zap:


## Technology stack

*   [PostgreSQL](https://www.postgresql.org/) - Open-Source Relational DBMS
*   [Java JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) - Java™ Platform, Standard Edition Development Kit
* 	[Spring Boot 2.3.4](https://spring.io/projects/spring-boot) - Open-Source Java Framework
* 	[pgAdmin](https://www.pgadmin.org/download/) - Open Source administration and development platform for PostgreSQL
* 	[Maven](https://maven.apache.org/) - Dependency Management
* 	[Hibernate](https://hibernate.org/orm/) - Object-relational mapping tool
* 	[Bucket4j](https://github.com/MarcGiffing/bucket4j-spring-boot-starter) - Rate limiting library based on token-bucket algorithm
* 	[git](https://git-scm.com/) - Free and Open-Source distributed version control system
* 	[Postman](https://www.getpostman.com/) - API Development Environment (Testing Docmentation)
* 	[Heroku](https://devcenter.heroku.com/articles/git) - Deploy management with git


The application is generated with [Spring Initializr](https://start.spring.io/)


## Usage

PostgreSQL, Java JDK 11 and pgAdmin4 should be installed before usage.:exclamation: :exclamation:

We recommend to download an IDE like [Intellij IDEA](https://www.jetbrains.com/idea/download/#section=windows) for building and running the application.

1. Open a command prompt window.

2. Navigate to the folder you want to store the application.

3. Clone the repository in this folder by doing the following command:

```
$ git clone git@gitlab.com:robinfroland/boxinator-db.git
```

4. Open the project in Intellij IDEA.


5. Open pgAdmin4 and create a server with all the database information from the postgresql install, and create a database.

<img src="/uploads/eb0c91cd0f3c20ffe239923e8a934018/create_db.PNG" alt="Create DB" width="50%" height="50%">

6. Now you need to change the database url, port, username and password in the same src\resources\application.properties file to match your database creterials.

7. Visit https://sendgrid.com/ and follow their guide in order to get an email and a sendgrid key.

8. Change the sendgrid email in the src\main\java\com\experis\boxinator\services\SendGridMailService.java file, and change the sendgrid key in the src\resources\application.properties file.

9. The backend application is now ready. Build and run the application.:boom:

<img src="/uploads/7dfd2acdbe90ef7bf530b07042fd0832/run_build_app.png" alt="Run app" width="70%" height="70%">



## Database ERD

<img src="/uploads/58c510e49f544ef2006062a7cd9fc11e/boxinator_ER.png" alt="Database ERD" width="70%" height="70%">


## API Documentation

Link to postman API documentation: https://documenter.getpostman.com/view/12300487/TVYGde5g


## Frontend repository

Frontend source-code repository and usage: https://gitlab.com/robinfroland/boxinator


## Deployed application

Link to deployed application: https://boxinator-app.herokuapp.com/


## Team members
Robin Frøland \
Pia Wold Nilsen \
Magne Kjellesvik \
Kristian Gyene


## License
[MIT](https://choosealicense.com/licenses/mit/)