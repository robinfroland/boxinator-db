package com.experis.boxinator.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "country_id")
    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Column(name = "fee_multiplier")
    private Double feeMultiplier;

    @OneToMany(mappedBy = "country")
    Set<Shipment> shipments = new HashSet<>();

    // TODO: Return endpoint results
    @JsonGetter("shipments")
    public Set<String> shipments(){
        return shipments.stream().map(shipment -> {
            return "/api/v1/shipments/" + shipment.getId();
        }).collect(Collectors.toSet());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getFeeMultiplier() {
        return feeMultiplier;
    }

    public void setFeeMultiplier(Double feeMultiplier) {
        this.feeMultiplier = feeMultiplier;
    }

    public Set<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(Set<Shipment> shipments) {
        this.shipments = shipments;
    }

    public void addNewShipment(Shipment shipment){
        this.shipments.add(shipment);
    }

    public void removeShipment(Shipment shipment){
        this.shipments.remove(shipment);
    }
}
