package com.experis.boxinator.models;

import javax.persistence.*;

@Entity
public class UniqueCode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "secret_key")
    private String secretKey;

    @Column(name = "validation_code")
    private Integer validationCode;

    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;


    public UniqueCode() {
    }

    public UniqueCode(String secretKey, Integer validationCode, Account account) {
        this.secretKey = secretKey;
        this.validationCode = validationCode;
        this.account = account;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Integer getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(Integer validationCode) {
        this.validationCode = validationCode;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
