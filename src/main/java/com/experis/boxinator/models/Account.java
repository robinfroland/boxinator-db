package com.experis.boxinator.models;

import com.experis.boxinator.utils.constants.AccountType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "account_id")
    private Long id;

    @Column
    private Boolean activated = false;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AccountType role;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column
    private String email;

    @Column
    private String password;

    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(name = "date_of_birth")
    private Date dob;

    @Column
    private String country;

    @Column(name = "zip_code")
    private String zipCode;

    @Column
    private String phone;

    @OneToMany(mappedBy = "account")
    Set<Shipment> shipments = new HashSet<>();

    @JsonGetter("shipments")
    public Set<String> shipments(){
        return shipments.stream().map(shipment ->
            "/api/v1/account/" + this.id + "/shipments/" + shipment.getId()
        ).collect(Collectors.toSet());
    }



    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountType getRole() {
        return role;
    }

    public void setRole(AccountType role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(Set<Shipment> shipments) {
        this.shipments = shipments;
    }

    public void addNewShipment(Shipment shipment){
        this.shipments.add(shipment);
    }

    public void removeShipment(Shipment shipment){
        this.shipments.remove(shipment);
    }
}
