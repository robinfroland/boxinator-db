package com.experis.boxinator.repositories;

import com.experis.boxinator.models.Account;
import com.experis.boxinator.models.UniqueCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UniqueCodeRepository extends JpaRepository<UniqueCode, Long> {
    UniqueCode findByAccountEmail(String email);
    UniqueCode findByAccount(Account account);
}
