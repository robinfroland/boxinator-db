package com.experis.boxinator.repositories;

import com.experis.boxinator.models.Account;
import com.experis.boxinator.models.Shipment;
import com.experis.boxinator.utils.constants.ShipmentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Long> {
    Shipment findShipmentByIdAndAccount_Id(Long shipmentId, Long accountId);
    List<Shipment> findAllByStatusLikeAndAccount_Id(ShipmentStatus status, Long id);

    // Get all active shipments
    List<Shipment> findAllByStatusLikeOrStatusLikeOrStatusLike(ShipmentStatus created, ShipmentStatus inTransit, ShipmentStatus received);

    // Get all active shipments sorted by date
    List<Shipment> findAllByStatusLikeOrStatusLikeOrStatusLikeOrderByDateCreatedDesc(ShipmentStatus created, ShipmentStatus inTransit, ShipmentStatus received);

    // Get all active shipments sorted by status
    List<Shipment> findAllByStatusLikeOrStatusLikeOrStatusLikeOrderByStatus(ShipmentStatus created, ShipmentStatus inTransit, ShipmentStatus received);

    // Get all completed/cancelled shipments
    List<Shipment> findAllByStatusLike(ShipmentStatus status);

    // Get all completed/cancelled shipments sorted by date
    List<Shipment> findAllByStatusLikeOrderByDateCreatedDesc(ShipmentStatus status);
}
