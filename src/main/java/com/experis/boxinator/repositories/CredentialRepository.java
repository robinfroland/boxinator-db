package com.experis.boxinator.repositories;

import com.experis.boxinator.models.Account;
import com.experis.boxinator.models.UniqueCode;
import com.experis.boxinator.utils.exceptions.NotFoundException;
import com.warrenstrange.googleauth.ICredentialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class CredentialRepository implements ICredentialRepository {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    UniqueCodeRepository uniqueCodeRepository;


    @Override
    public String getSecretKey(String email) {
        return uniqueCodeRepository.findByAccountEmail(email).getSecretKey();
    }

    @Override
    public void saveUserCredentials(String email,
                                    String secretKey,
                                    int validationCode,
                                    List<Integer> scratchCodes) {

        Account existingAccount = accountRepository.findByEmail(email);
        if (existingAccount == null) throw new NotFoundException("Could not find account with email " + email, 0L);

        UniqueCode existingCode = uniqueCodeRepository.findByAccountEmail(email);

        if(existingCode == null){
            uniqueCodeRepository.save(new UniqueCode(secretKey, validationCode, existingAccount));
        }
        else{
            existingCode.setSecretKey(secretKey);
            existingCode.setValidationCode(validationCode);
            uniqueCodeRepository.save(existingCode);
        }
    }
}