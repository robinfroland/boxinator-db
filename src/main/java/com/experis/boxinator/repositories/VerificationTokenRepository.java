package com.experis.boxinator.repositories;

import com.experis.boxinator.models.Account;
import com.experis.boxinator.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    VerificationToken findByToken(String token);
    VerificationToken findByAccount(Account account);
}
