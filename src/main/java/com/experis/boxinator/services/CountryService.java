package com.experis.boxinator.services;

import com.experis.boxinator.models.Country;
import com.experis.boxinator.repositories.CountryRepository;
import com.experis.boxinator.utils.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;


    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER', 'GUEST')")
    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }


    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER', 'GUEST')")
    public Country getCountry(Long id) {
        return countryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Country not found with id: ", id));
    }


    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Country addCountry(Country country) {
        return countryRepository.save(country);
    }


    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Country updateCountry(Country country, Long id) {
        Country existingCountry = countryRepository.findById(country.getId())
                .orElseThrow(() -> new NotFoundException("Country not found with id: ", id));

        if (country.getFeeMultiplier() != null) existingCountry.setFeeMultiplier(country.getFeeMultiplier());

        return countryRepository.save(existingCountry);
    }
}
