package com.experis.boxinator.services;

import com.experis.boxinator.models.Account;
import com.experis.boxinator.models.AccountDetails;
import com.experis.boxinator.models.Country;
import com.experis.boxinator.models.Shipment;
import com.experis.boxinator.repositories.AccountRepository;
import com.experis.boxinator.repositories.PackageRepository;
import com.experis.boxinator.repositories.ShipmentRepository;
import com.experis.boxinator.repositories.VerificationTokenRepository;
import com.experis.boxinator.utils.constants.AccountType;
import com.experis.boxinator.utils.constants.ShipmentStatus;
import com.experis.boxinator.utils.exceptions.BadRequestException;
import com.experis.boxinator.utils.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShipmentService {

    private static final int SHIPPING_FEE = 69;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private PackageRepository packageRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private SendGridMailService mailService;

    @Autowired
    private VerificationTokenRepository tokenRepository;


    /**
     * Gets active shipments based on authentication
     * Admin can retrieve all active shipments
     * Registered users can only get their own active shipments
     */
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER')")
    @PostFilter("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and filterObject.account.id == authentication.principal.id)")
    public List<Shipment> getAllShipments(String sort) {
        List<Shipment> activeShipments = new ArrayList<>();

        if (sort != null && sort.equals("date")){
            activeShipments.addAll(shipmentRepository.findAllByStatusLikeOrStatusLikeOrStatusLikeOrderByDateCreatedDesc
                    (ShipmentStatus.CREATED, ShipmentStatus.IN_TRANSIT, ShipmentStatus.RECEIVED));
        } else if (sort != null && sort.equals("status")){
            activeShipments.addAll(shipmentRepository.findAllByStatusLikeOrStatusLikeOrStatusLikeOrderByStatus
                    (ShipmentStatus.CREATED, ShipmentStatus.IN_TRANSIT, ShipmentStatus.RECEIVED));
        } else {
            activeShipments.addAll(shipmentRepository.findAllByStatusLikeOrStatusLikeOrStatusLike
                    (ShipmentStatus.CREATED, ShipmentStatus.IN_TRANSIT, ShipmentStatus.RECEIVED));
        }

        return activeShipments;
    }


    /**
     * Gets completed shipments based on authentication
     * Admin can retrieve all completed shipments
     * Registered users can only get their own completed shipments
     */
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER')")
    @PostFilter("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and filterObject.account.id == authentication.principal.id)")
    public List<Shipment> getCompleteShipments(String sort) {
        List<Shipment> completedShipments = new ArrayList<>();

        if (sort != null && sort.equals("date")){
            completedShipments.addAll(shipmentRepository.findAllByStatusLikeOrderByDateCreatedDesc
                    (ShipmentStatus.COMPLETED));
        } else {
            completedShipments.addAll(shipmentRepository.findAllByStatusLike
                    (ShipmentStatus.COMPLETED));
        }

        return completedShipments;
    }


    /**
     * Gets cancelled shipments based on authentication
     * Admin can retrieve all cancelled shipments
     * Registered users can only get their own cancelled shipments
     */
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER')")
    @PostFilter("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and filterObject.account.id == authentication.principal.id)")
    public List<Shipment> getCancelledShipments(String sort) {
        List<Shipment> cancelledShipments = new ArrayList<>();

        if (sort != null && sort.equals("date")){
            cancelledShipments.addAll(shipmentRepository.findAllByStatusLikeOrderByDateCreatedDesc
                    (ShipmentStatus.CANCELLED));
        } else {
            cancelledShipments.addAll(shipmentRepository.findAllByStatusLike
                    (ShipmentStatus.CANCELLED));
        }

        return cancelledShipments;

    }


    /**
     * Admin can retrieve shipments made by other accounts,
     * but a registered user can only access their own shipment
     */
    @PostAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and returnObject.account.id == authentication.principal.id)")
    public Shipment getShipment(Long id) {
        return shipmentRepository.findById(id).orElseThrow(() -> new NotFoundException("Shipment not found with id: ", id));
    }


    /**
     * Admin can retrieve shipments made by other accounts,
     * but a registered user can only access their own shipment
     */
    @PostAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and returnObject.account.id == authentication.principal.id)")
    public Shipment getCompleteShipment(Long id) {
        Shipment completedShipment = shipmentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Shipment not found with id: ", id));

        if (completedShipment.getStatus() == ShipmentStatus.COMPLETED) {
            return completedShipment;
        } else {
            throw new BadRequestException("Shipment found, but is not complete. Provided id: ", id);
        }
    }


    /**
     * Add new shipment and set the related account based on authentication
     */
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER', 'GUEST')")
    public Shipment addShipment(Shipment shipment) {
        AccountDetails accountDetails = (AccountDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account existingAccount = accountRepository.findById(accountDetails.getId())
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", accountDetails.getId()));
        packageRepository.save(shipment.getShipmentPackage());
        calculateTotalShippingPrice(shipment);
        shipment.setAccount(existingAccount);

        if (existingAccount.getRole() == AccountType.GUEST) {
            mailService.sendGuestReceiptMail(existingAccount.getEmail(), tokenRepository.findByAccount(existingAccount).getToken());
        }

        return shipmentRepository.save(shipment);
    }


    /**
     * Updates shipment.
     * Admin can change account, all statuses and country of the shipment
     * Registered user can only change shipment status to CANCELLED
     */
    @PreAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and #shipment.account.id == authentication.principal.id)")
    public Shipment updateShipment(Shipment shipment) {
        Shipment existingShipment = shipmentRepository.findById(shipment.getId())
                .orElseThrow(() -> new NotFoundException("Shipment not found with id: ", shipment.getId()));

        List<String> roles = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        String role = roles.get(0).split("_", 2)[1];

        if(role.equals("ADMINISTRATOR")){
            if (shipment.getAccount() != null) existingShipment.setAccount(shipment.getAccount());
            if (shipment.getStatus() != null) existingShipment.setStatus(shipment.getStatus());
            if (shipment.getCountry() != null){
                existingShipment.setCountry(shipment.getCountry());
                calculateTotalShippingPrice(existingShipment);
            }
        } else if(role.equals("REGISTERED_USER")){
            if (shipment.getStatus() != null)
                if (!shipment.getStatus().equals(ShipmentStatus.CANCELLED)){
                    throw new BadRequestException("Can only change status to CANCELLED. Provided shipment id: ", shipment.getId());
                }
                existingShipment.setStatus(shipment.getStatus());
        }

        return shipmentRepository.save(existingShipment);
    }


    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteShipment(Long id){
        Shipment shipment = shipmentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find shipment with id: ", id));

        shipment.getAccount().removeShipment(shipment);
        shipment.getCountry().removeShipment(shipment);

        shipmentRepository.delete(shipment);
    }

    private Shipment calculateTotalShippingPrice(Shipment shipment) {
        Country country = countryService.getCountry(shipment.getCountry().getId());
        shipment.setTotalPrice((int) (SHIPPING_FEE + (shipment.getShipmentPackage().getWeight() * country.getFeeMultiplier())));
        return shipment;
    }
}
