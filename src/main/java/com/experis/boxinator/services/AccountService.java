package com.experis.boxinator.services;

import com.experis.boxinator.models.*;
import com.experis.boxinator.repositories.AccountRepository;
import com.experis.boxinator.repositories.ShipmentRepository;
import com.experis.boxinator.repositories.UniqueCodeRepository;
import com.experis.boxinator.repositories.VerificationTokenRepository;
import com.experis.boxinator.utils.constants.AccountType;
import com.experis.boxinator.utils.constants.ShipmentStatus;
import com.experis.boxinator.utils.exceptions.BadRequestException;
import com.experis.boxinator.utils.exceptions.NotFoundException;
import com.experis.boxinator.utils.exceptions.AccountAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class AccountService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private UniqueCodeRepository uniqueCodeRepository;

    @Autowired
    private PasswordEncoder encoder;



    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }


    /**
     * Get account details based on authentication
     */
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER')")
    public Account getAccountDetails() {
        AccountDetails accountDetails = (AccountDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return accountRepository.findById(accountDetails.getId())
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", accountDetails.getId()));
    }


    /**
     * Does not require authentication
     * Used when updating from guest to registered user
     */
    public Account getGuestDetails(String token){
        VerificationToken verificationToken = tokenRepository.findByToken(token);
        if (verificationToken == null) {
            throw new BadRequestException("Verification token does not exist or is expired", 0L);
        }

        Account existingAccount = verificationToken.getAccount();
        if (existingAccount == null) {
            throw new NotFoundException("Could not find account with token. No id to display.", 0L);
        }
        return existingAccount;
    }


    @PreAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and #id == authentication.principal.id)")
    public Account getAccount(Long id){
        return accountRepository.findById(id).orElseThrow(() -> new NotFoundException("Could not find account with id: ", id));
    }


    @PreAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and #id == authentication.principal.id)")
    public Set<Shipment> getCustomerShipments(Long id) {
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", id));
        return account.getShipments();
    }


    @PreAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and #id == authentication.principal.id)")
    public List<Shipment> getCompleteCustomerShipments(Long id) {
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", id));

        return new ArrayList<>(shipmentRepository.findAllByStatusLikeAndAccount_Id(ShipmentStatus.COMPLETED, id));
    }


    @PreAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and #customerId == authentication.principal.id)")
    public Shipment getCustomerShipment(Long customerId, Long shipmentId) {
        accountRepository.findById(customerId)
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", customerId));
        shipmentRepository.findById(shipmentId)
                .orElseThrow(() -> new NotFoundException("Could not find shipment with id: ", shipmentId));

        return shipmentRepository.findShipmentByIdAndAccount_Id(shipmentId, customerId);
    }


    public Account addAccount(Account account) {
        if (accountRepository.findByEmail(account.getEmail()) != null) {
            throw new AccountAlreadyExistException("Account is already registered with email: ", account.getEmail());
        }
        if (account.getPassword() != null) {
            account.setPassword(encoder.encode(account.getPassword()));
        }
        return accountRepository.save(account);
    }


    /**
     * Updates account based on authentication
     */
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'REGISTERED_USER')")
    public Account updateAccount(Account account){
        AccountDetails accountDetails = (AccountDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account existingAccount = accountRepository.findById(accountDetails.getId())
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", account.getId()));

        setAccountValues(account, existingAccount);
        return accountRepository.save(existingAccount);
    }


    /**
     * Updates account based on id.
     * Can be used for future features where administrators can upgrade registered users to be administrators.
     */
    @PreAuthorize("hasRole('ADMINISTRATOR') or (hasRole('REGISTERED_USER') and #id == authentication.principal.id)")
    public Account updateAccount(Account account, Long id){
        Account existingAccount = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", account.getId()));
        setAccountValues(account, existingAccount);
        return accountRepository.save(existingAccount);
    }


    /**
     * Register and activate guest account
     */
    public Account registerGuest(Account account){
        Account existingAccount = accountRepository.findByEmail(account.getEmail());
        if (existingAccount == null) throw new NotFoundException("Could not find account with email " + account.getEmail(), 0L);

        setAccountValues(account, existingAccount);
        existingAccount.setRole(AccountType.REGISTERED_USER);
        existingAccount.setActivated(true);
        return accountRepository.save(existingAccount);
    }


    /**
     * Updates account values for PUT requests
     */
    private void setAccountValues(Account account, Account existingAccount){
        if(account.getRole() != null) existingAccount.setRole(account.getRole());
        if(account.getEmail() != null) existingAccount.setEmail(account.getEmail());
        if(account.getFirstName() != null) existingAccount.setFirstName(account.getFirstName());
        if(account.getLastName() != null) existingAccount.setLastName(account.getLastName());
        if(account.getPassword() != null) existingAccount.setPassword(encoder.encode(account.getPassword()));
        if(account.getDob() != null) existingAccount.setDob(account.getDob());
        if(account.getCountry() != null) existingAccount.setCountry(account.getCountry());
        if(account.getZipCode() != null) existingAccount.setZipCode(account.getZipCode());
        if(account.getPhone() != null) existingAccount.setPhone(account.getPhone());
    }


    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteAccount(Long id){
        Account existingAccount = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find account with id: ", id));

        VerificationToken verificationToken = tokenRepository.findByAccount(existingAccount);
        if (verificationToken != null) tokenRepository.delete(verificationToken);

        UniqueCode uniqueCode = uniqueCodeRepository.findByAccount(existingAccount);
        if (uniqueCode != null) uniqueCodeRepository.delete(uniqueCode);

        for (Shipment shipment: existingAccount.getShipments()) {
            shipment.setAccount(null);
        }

        accountRepository.delete(existingAccount);
    }


    public void addVerificationToken(Account account, String token) {
        VerificationToken verificationToken = new VerificationToken(token, account);
        tokenRepository.save(verificationToken);
    }


    public boolean tokenIsValid(String token) {
        VerificationToken verificationToken = tokenRepository.findByToken(token);

        if (verificationToken == null) {
            return false;
        }

        if ((verificationToken.getExpiryDate()
                .getTime() - Calendar.getInstance().getTime()
                .getTime()) <= 0) {
            tokenRepository.delete(verificationToken);
            return false;
        }
        return true;
    }


    public void activateAccount(String token) {
        Account account = tokenRepository.findByToken(token).getAccount();
        account.setActivated(true);
        accountRepository.save(account);
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(email);
        if (account == null) {
            throw new UsernameNotFoundException("Account not found with email: " + email);
        }
        return AccountDetails.build(account);
    }
}
