package com.experis.boxinator.services;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static com.experis.boxinator.utils.constants.Constants.FE_BASE_URL;

@Service
public class SendGridMailService {

    SendGrid sendGrid;

    @Autowired
    public SendGridMailService(SendGrid sendGrid) {
        this.sendGrid = sendGrid;
    }

    public void sendMail(String mailRecipient, String token) {
        String verificationUrl = FE_BASE_URL + "/verify?token=" + token;

        Email from = new Email("boxinator.app@gmail.com");
        String subject = "Welcome to Boxinator!";
        Email to = new Email(mailRecipient);

        Mail mail = new Mail();
        mail.setFrom(from);
        mail.setSubject(subject);
        mail.setTemplateId("d-95ece0f816984bccbfdc45d22cf83174");

        Personalization personalization = new Personalization();
        personalization.addTo(to);
        personalization.addDynamicTemplateData("verificationUrl", verificationUrl);
        mail.addPersonalization(personalization);

        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("/mail/send");
            request.setBody(mail.build());
            sendGrid.api(request);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void sendGuestReceiptMail(String mailRecipient, String token) {
        String verificationUrl = FE_BASE_URL + "/register?token=" + token;

        Email from = new Email("boxinator.app@gmail.com");
        String subject = "Shipment receipt";
        Email to = new Email(mailRecipient);

        Mail mail = new Mail();
        mail.setFrom(from);
        mail.setSubject(subject);
        mail.setTemplateId("d-c0189143298a403984d009e416e6c5f2");

        Personalization personalization = new Personalization();
        personalization.addTo(to);
        personalization.addDynamicTemplateData("verificationUrl", verificationUrl);
        mail.addPersonalization(personalization);

        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("/mail/send");
            request.setBody(mail.build());
            sendGrid.api(request);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
