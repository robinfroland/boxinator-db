package com.experis.boxinator.services;

import com.experis.boxinator.models.AccountDetails;
import com.experis.boxinator.security.jwt.JwtRequest;
import com.experis.boxinator.security.jwt.JwtResponse;
import com.experis.boxinator.security.jwt.JwtUtils;
import com.experis.boxinator.utils.constants.AccountType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    public JwtResponse authenticateGuest(JwtRequest jwtRequest){
        // Authenticate the user email from the request.
        Authentication authentication = new UsernamePasswordAuthenticationToken(jwtRequest.getEmail(), null,
                AuthorityUtils.createAuthorityList("ROLE_GUEST"));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String role = AccountType.GUEST.toString();

        // Generated token with the format 'Authorization: Bearer <token>'
        String jwtToken = jwtUtils.generateJwtToken(jwtRequest.getEmail());

        return new JwtResponse("Guest", jwtToken, role);
    }

    public JwtResponse authenticateUser(JwtRequest jwtRequest){
        // Authenticate the user email and password from the request.
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(jwtRequest.getEmail(), jwtRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Extract user role.
        AccountDetails accountDetails = (AccountDetails) authentication.getPrincipal();
        String role = accountDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()).get(0).split("_", 2)[1];

        // Generated token with the format 'Authorization: Bearer <token>'
        String jwtToken = jwtUtils.generateJwtToken(jwtRequest.getEmail());

        if(!accountDetails.getActivated()){ // Check if the account has been activated with register email
            return new JwtResponse("Account is not activated.");
        }
        return new JwtResponse(accountDetails.getName(), jwtToken, role);

    }
}
