package com.experis.boxinator.registration;

import java.util.UUID;
import com.experis.boxinator.models.Account;
import com.experis.boxinator.services.AccountService;
import com.experis.boxinator.services.SendGridMailService;
import com.experis.boxinator.utils.constants.AccountType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    @Autowired
    private AccountService accountService;

    @Autowired
    private SendGridMailService mailService;

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    // Add verification token and send confirmation email
    private void confirmRegistration(final OnRegistrationCompleteEvent event) {
        Account account = event.getAccount();
        String token = UUID.randomUUID().toString();
        accountService.addVerificationToken(account, token);

        if (account.getRole() != AccountType.GUEST){
            mailService.sendMail(account.getEmail(), token);
        }
    }
}
