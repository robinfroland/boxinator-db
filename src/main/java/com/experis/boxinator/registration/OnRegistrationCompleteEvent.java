package com.experis.boxinator.registration;

import com.experis.boxinator.models.Account;
import org.springframework.context.ApplicationEvent;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private final Account account;

    public OnRegistrationCompleteEvent(Account account) {
        super(account);
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }
}
