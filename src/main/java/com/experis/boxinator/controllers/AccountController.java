package com.experis.boxinator.controllers;

import com.experis.boxinator.models.Account;
import com.experis.boxinator.models.Shipment;
import com.experis.boxinator.registration.OnRegistrationCompleteEvent;
import com.experis.boxinator.services.AccountService;
import com.experis.boxinator.utils.constants.AccountType;
import com.experis.boxinator.utils.exceptions.AccountAlreadyExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://boxinator-app.herokuapp.com/"}, allowedHeaders = {"Authorization"})
@RequestMapping("/api/v1/")
public class AccountController {

    @Autowired
    private AccountService accountService;


    @Autowired
    private ApplicationEventPublisher eventPublisher;

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);


    /**
     * Gets all accounts
     *
     * @return List of Accounts
     */
    @GetMapping("accounts")
    public ResponseEntity<List<Account>> getAllAccounts() {
        List<Account> accounts = accountService.getAllAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }


    /**
     * Get account details based on authentication
     * Uses token to get account details when registering a guest
     *
     * @param token - VerificationToken
     * @return Account
     */
    @GetMapping("account")
    public ResponseEntity<Account> getAccountDetails(@RequestParam(required = false) String token) {
        Account account;

        if (token != null) {
            account = accountService.getGuestDetails(token);
        } else {
            account = accountService.getAccountDetails();
        }
        return new ResponseEntity<>(account, HttpStatus.OK);
    }


    /**
     * Gets account details based on id
     *
     * @param id - account id
     * @return Account
     */
    @GetMapping("account/{id}")
    public ResponseEntity<Account> getAccount(@PathVariable Long id){
        Account account = accountService.getAccount(id);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }


    /**
     * Gets all shipments for account based on id
     *
     * @param id - account id
     * @return Set of Shipments
     */
    @GetMapping("account/{id}/shipments")
    public ResponseEntity<Set<Shipment>> getCustomerShipments(@PathVariable Long id){
        Set<Shipment> customerShipments = accountService.getCustomerShipments(id);

        return new ResponseEntity<>(customerShipments, HttpStatus.OK);
    }


    /**
     * Gets all completed shipments for account based on id
     *
     * @param id - account id
     * @return List of Shipments
     */
    @GetMapping("account/{id}/shipments/complete")
    public ResponseEntity<List<Shipment>> getCompleteCustomerShipments(@PathVariable Long id){
        List<Shipment> customerCompletedShipments = accountService.getCompleteCustomerShipments(id);

        return new ResponseEntity<>(customerCompletedShipments, HttpStatus.OK);
    }


    /**
     * Gets specific shipment for an account based on provided id
     * @param accountId - account id
     * @param shipmentId - shipment id
     * @return Shipment
     */
    @GetMapping("account/{accountId}/shipments/{shipmentId}")
    public ResponseEntity<Shipment> getCustomerShipment(@PathVariable Long accountId, @PathVariable Long shipmentId){
        HttpStatus status = HttpStatus.OK;
        Shipment customerShipment = accountService.getCustomerShipment(accountId, shipmentId);

        if (customerShipment == null){
            status = HttpStatus.FORBIDDEN;
        }

        return new ResponseEntity<>(customerShipment, status);
    }


    /**
     * Adds new account
     * Sends registration email when added
     *
     * @param account - account request body
     * @return Account
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PostMapping("account")
    public ResponseEntity<Account> addAccount(@RequestBody Account account){
        try {
            Account newAccount = accountService.addAccount(account);
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(newAccount));
        } catch (AccountAlreadyExistException alreadyExistException) {
            logger.error(alreadyExistException.getMessage());
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(account, HttpStatus.CREATED);
    }


    /**
     * Updates account based on authentication
     * If account role is GUEST, it will register the account to role REGISTERED_USER
     *
     * @param account - account request body
     * @return Updated Account
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PutMapping("account")
    public ResponseEntity<Account> updateAccount(@RequestBody Account account){
        Account updatedAccount;
        if (account.getRole() == AccountType.GUEST) {
            updatedAccount = accountService.registerGuest(account);
        } else {
            updatedAccount = accountService.updateAccount(account);
        }
        return new ResponseEntity<>(updatedAccount, HttpStatus.OK);
    }


    /**
     * Updates account based on id
     *
     * @param account - account request body
     * @param id - account id
     * @return Updated Account
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PutMapping("account/{id}")
    public ResponseEntity<Account> updateAccountById(@RequestBody Account account, @PathVariable Long id){
        Account updatedAccount = accountService.updateAccount(account, id);
        return new ResponseEntity<>(updatedAccount, HttpStatus.OK);
    }


    /**
     * Deletes account based on id
     *
     * @param id - account id
     * @return ResponseEntity<HttpStatus>
     */
    @DeleteMapping("account/{id}")
    public ResponseEntity<HttpStatus> deleteAccount(@PathVariable Long id){
        accountService.deleteAccount(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }



    /**
     * Activates the account upon registration
     *
     * @param token - VerificationToken
     * @return ResponseEntity<String>
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PutMapping("account/verify")
    public ResponseEntity<String> confirmRegistration(@RequestParam String token) {
        if (!accountService.tokenIsValid(token)) {
            return new ResponseEntity<>("Verification token does not exist or is expired", HttpStatus.UNAUTHORIZED);
        }
        accountService.activateAccount(token);
        return new ResponseEntity<>("Account successfully activated!", HttpStatus.OK);
    }
}
