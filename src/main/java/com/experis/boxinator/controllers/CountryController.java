package com.experis.boxinator.controllers;

import com.experis.boxinator.models.Country;
import com.experis.boxinator.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://boxinator-app.herokuapp.com/"}, allowedHeaders = {"Authorization"})
@RequestMapping("/api/v1/settings/countries")
public class CountryController {

    @Autowired
    private CountryService countryService;


    /**
     * Gets all countries
     *
     * @return List of Countries
     */
    @GetMapping()
    public ResponseEntity<List<Country>> getAllCountries(){
        List<Country> countries = countryService.getAllCountries();

        return new ResponseEntity<>(countries, HttpStatus.OK);
    }


    /**
     * Gets country based on id
     *
     * @param id - country id
     * @return Country
     */
    @GetMapping("/{id}")
    public ResponseEntity<Country> getCountry(@PathVariable Long id){
        Country country = countryService.getCountry(id);

        return new ResponseEntity<>(country, HttpStatus.OK);
    }


    /**
     * Adds new country
     *
     * @param country - country request body
     * @return Country
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PostMapping()
    public ResponseEntity<Country> addCountry(@RequestBody Country country){
        Country newCountry = countryService.addCountry(country);

        return new ResponseEntity<>(newCountry, HttpStatus.CREATED);
    }


    /**
     * Updates country based on id
     *
     * @param country - country request body
     * @param id - country id
     * @return Updated country
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PutMapping("/{id}")
    public ResponseEntity<Country> updateCountry(@RequestBody Country country, @PathVariable Long id){
        if (country.getId() == null || !country.getId().equals(id)){
            return new ResponseEntity<>(new Country(), HttpStatus.BAD_REQUEST);
        }

        Country updatedCountry = countryService.updateCountry(country, id);

        return new ResponseEntity<>(updatedCountry, HttpStatus.OK);
    }
}
