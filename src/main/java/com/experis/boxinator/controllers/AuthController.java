package com.experis.boxinator.controllers;

import com.experis.boxinator.models.Account;
import com.experis.boxinator.repositories.AccountRepository;
import com.experis.boxinator.security.totp.TotpRequest;
import com.experis.boxinator.security.totp.TotpResponse;
import com.experis.boxinator.security.jwt.JwtRequest;
import com.experis.boxinator.security.jwt.JwtResponse;
import com.experis.boxinator.services.AuthService;
import com.experis.boxinator.utils.exceptions.NotFoundException;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://boxinator-app.herokuapp.com/"}, allowedHeaders = {"Content-Type"})
@RequestMapping("/api/v1")
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private GoogleAuthenticator gAuth;

    public static String QR_PREFIX = "https://chart.googleapis.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=";

    /**
     * Login with email and password
     * If no password is not provided, it will authenticate the guest without password
     *
     * @param jwtRequest - contains email and password
     * @return JwtResponse with name, token and role
     */
    @PostMapping("/login")
    public ResponseEntity<JwtResponse> authenticateUser(@RequestBody JwtRequest jwtRequest){

        if (jwtRequest.getPassword() == null) {
            return new ResponseEntity<>(authService.authenticateGuest(jwtRequest), HttpStatus.OK);
        }

        JwtResponse jwtResponse = authService.authenticateUser(jwtRequest);
        if (jwtResponse.getToken() != null) {
            return new ResponseEntity<>(jwtResponse, HttpStatus.OK);
        }
        return new ResponseEntity<>(jwtResponse, HttpStatus.FORBIDDEN);
    }


    @PostMapping("/2fa/generate")
    public ResponseEntity<TotpResponse> generateQR(@RequestBody TotpRequest totpRequest) {
        final GoogleAuthenticatorKey key = gAuth.createCredentials(totpRequest.getEmail());

        // Generating QR code for user to scan.
        String otpAuthURL = GoogleAuthenticatorQRGenerator.getOtpAuthTotpURL("Boxinator", totpRequest.getEmail(), key);
        String url =  QR_PREFIX + otpAuthURL;

        return new ResponseEntity<>(new TotpResponse(url), HttpStatus.OK);
    }


    @PostMapping("/2fa/validate")
    public ResponseEntity<TotpResponse> validateKey(@RequestBody TotpRequest request) {
        Account existingAccount = accountRepository.findByEmail(request.getEmail());
        if (existingAccount == null) throw new NotFoundException("Could not find account with email " + request.getEmail(), 0L);

        return new ResponseEntity<>(new TotpResponse(gAuth.authorizeUser(request.getEmail(), request.getCode())), HttpStatus.OK);
    }
}
