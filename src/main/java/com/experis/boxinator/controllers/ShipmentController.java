package com.experis.boxinator.controllers;

import com.experis.boxinator.models.Shipment;
import com.experis.boxinator.services.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "https://boxinator-app.herokuapp.com/"}, allowedHeaders = {"Authorization"})
@RequestMapping("/api/v1/shipments")
public class ShipmentController {

    @Autowired
    private ShipmentService shipmentService;


    /**
     * Gets all active shipments
     * Active shipments have status like CREATED, IN_TRANSIT or RECEIVED.
     *
     * @param sort - can optionally sort by 'date' or 'status' to sort shipments
     * @return List of Shipments
     */
    @GetMapping()
    public ResponseEntity<List<Shipment>> getAllShipments(@RequestParam(required = false) String sort){
        List<Shipment> shipments = shipmentService.getAllShipments(sort);

        return new ResponseEntity<>(shipments, HttpStatus.OK);
    }


    /**
     * Gets all completed shipments
     *
     * @param sort - can optionally sort by 'date'
     * @return List of Shipments
     */
    @GetMapping("/complete")
    public ResponseEntity<List<Shipment>> getCompleteShipments(@RequestParam(required = false) String sort){
        List<Shipment> completeShipments = shipmentService.getCompleteShipments(sort);

        return new ResponseEntity<>(completeShipments, HttpStatus.OK);
    }


    /**
     * Gets all cancelled shipments
     *
     * @param sort - can optionally sort by 'date'
     * @return List of Shipments
     */
    @GetMapping("/cancelled")
    public ResponseEntity<List<Shipment>> getCancelledShipments(@RequestParam(required = false) String sort){
        List<Shipment> cancelledShipments = shipmentService.getCancelledShipments(sort);

        return new ResponseEntity<>(cancelledShipments, HttpStatus.OK);
    }



    /**
     * Get shipment based on id
     *
     * @param shipmentId - shipment id
     * @return Shipment
     */
    @GetMapping("/{shipmentId}")
    public ResponseEntity<Shipment> getShipment(@PathVariable Long shipmentId){
        Shipment shipment = shipmentService.getShipment(shipmentId);

        return new ResponseEntity<>(shipment, HttpStatus.OK);
    }


    /**
     * Gets complete shipment based on id
     *
     * @param shipmentId - shipment id
     * @return Completed shipment
     */
    @GetMapping("/complete/{shipmentId}")
    public ResponseEntity<Shipment> getCompleteShipment(@PathVariable Long shipmentId){
        Shipment completeShipment = shipmentService.getCompleteShipment(shipmentId);

        return new ResponseEntity<>(completeShipment, HttpStatus.OK);
    }


    /**
     * Adds new shipment
     *
     * @param shipment - shipment request body
     * @return Shipment
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PostMapping()
    public ResponseEntity<Shipment> addShipment(@RequestBody Shipment shipment){
        Shipment newShipment = shipmentService.addShipment(shipment);

        return new ResponseEntity<>(newShipment, HttpStatus.CREATED);
    }


    /**
     * Updates shipment based on id
     *
     * @param shipment - shipment request body
     * @param shipmentId - shipment id
     * @return Updated Shipment
     */
    @CrossOrigin(allowedHeaders = {"Content-Type"})
    @PutMapping("/{shipmentId}")
    public ResponseEntity<Shipment> updateShipment(@RequestBody Shipment shipment, @PathVariable Long shipmentId){
        if (!shipment.getId().equals(shipmentId)){
            return new ResponseEntity<>(new Shipment(), HttpStatus.BAD_REQUEST);
        }

        Shipment updatedShipment = shipmentService.updateShipment(shipment);
        return new ResponseEntity<>(updatedShipment, HttpStatus.OK);
    }


    /**
     * Deletes shipment based on id
     *
     * @param shipmentId - shipment id
     * @return ResponseEntity<HttpStatus>
     */
    @DeleteMapping("/{shipmentId}")
    public ResponseEntity<HttpStatus> deleteShipment(@PathVariable Long shipmentId){
        shipmentService.deleteShipment(shipmentId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
