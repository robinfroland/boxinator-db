package com.experis.boxinator.utils.constants;

public enum ShipmentStatus {
    CREATED,
    RECEIVED,
    IN_TRANSIT,
    COMPLETED,
    CANCELLED
}
