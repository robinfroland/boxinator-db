package com.experis.boxinator.utils.constants;

public enum AccountType {
    GUEST,
    REGISTERED_USER,
    ADMINISTRATOR
}
