package com.experis.boxinator.utils.constants;

public final class Constants {

    public static final int API_VERSION = 1;

    // TODO: Replace with deployed urls
    public static final String DB_BASE_URL = "https://boxinator-app-db.herokuapp.com/api/v" + API_VERSION;
    public static final String FE_BASE_URL = "https://boxinator-app.herokuapp.com";
}
