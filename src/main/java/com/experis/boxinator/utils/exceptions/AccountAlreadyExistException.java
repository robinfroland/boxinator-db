package com.experis.boxinator.utils.exceptions;

public final class AccountAlreadyExistException extends RuntimeException {

    public AccountAlreadyExistException(String message, String email) {
        super(message + email);
    }

}
