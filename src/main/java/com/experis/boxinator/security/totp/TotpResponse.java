package com.experis.boxinator.security.totp;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TotpResponse {
    private Boolean valid = null;
    private String url;


    public TotpResponse(boolean valid) {
        this.valid = valid;
    }

    public TotpResponse(String url) {
        this.url = url;
    }

    public Boolean isValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
