package com.experis.boxinator.security;

import com.experis.boxinator.security.jwt.JwtUtils;
import com.experis.boxinator.models.AccountDetails;
import com.experis.boxinator.services.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthTokenFilter extends OncePerRequestFilter {

    private JwtUtils jwtUtils;

    private AccountService accountService;

    public AuthTokenFilter(JwtUtils jwtUtils, AccountService accountService) {
        this.jwtUtils = jwtUtils;
        this.accountService = accountService;
    }

    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    // Validating the token, and gets account details based on the email extracted from the token.
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        try {
            String token = parseJwt(request);

            if(token != null && jwtUtils.validateToken(token)){ // Check if the token is valid.
                String email = jwtUtils.getUserNameFromJwtToken(token); // Extract the email from the token.

                // Get account details from the authenticated account.
                AccountDetails accountDetails = (AccountDetails) accountService.loadUserByUsername(email);

                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(accountDetails,
                        null, accountDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                // Set account details in SecurityContext
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        } catch (Exception e){
            logger.error("Cannot set user authentication:" + e);
        }
        filterChain.doFilter(request, response);
    }

    private String parseJwt(HttpServletRequest request){
        String header = request.getHeader("Authorization");

        if (StringUtils.hasText(header) && header.startsWith("Bearer ")) {
            return header.substring(7);
        }

        return null;
    }
}
