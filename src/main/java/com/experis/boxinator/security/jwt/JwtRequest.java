package com.experis.boxinator.security.jwt;

import com.sun.istack.NotNull;

public class JwtRequest {

    @NotNull
    private String email;

    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
