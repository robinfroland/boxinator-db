package com.experis.boxinator.security.jwt;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class JwtResponse {
    private String name;
    private String token;
    private String role;
    private String message;

    public JwtResponse(String name, String token, String role) {
        this.name = name;
        this.token = token;
        this.role = role;
    }

    public JwtResponse(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
