package com.experis.boxinator.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthEntryPoint implements AuthenticationEntryPoint {
    private static final Logger logger = LoggerFactory.getLogger(AuthEntryPoint.class);


    // Will be triggered when an unauthenticated account requests an API endpoint.
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
    throws IOException {
        logger.error("Unauthorized error: {}", exception.getMessage());

        // Send 401 status code.
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error: unauthorized.");
    }
}
