package com.experis.boxinator.controllers;

import com.experis.boxinator.models.Country;
import com.experis.boxinator.models.Shipment;
import com.experis.boxinator.repositories.CountryRepository;
import com.experis.boxinator.repositories.ShipmentRepository;
import com.experis.boxinator.security.jwt.JwtUtils;
import com.experis.boxinator.utils.constants.ShipmentStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ShipmentControllerTest {


    @Autowired
    private MockMvc mvc;

    @Autowired
    private JwtUtils jwtUtils;

    @MockBean
    private ShipmentRepository shipmentRepository;

    @Autowired
    private ObjectMapper objectMapper;


    private final List<Shipment> shipmentList = new ArrayList<>();


    @BeforeEach
    void setUp() {

        Shipment shipment1 = new Shipment();
        shipment1.setId(1L);
        shipment1.setStatus(ShipmentStatus.CREATED);
        shipmentList.add(shipment1);

        Shipment shipment2 = new Shipment();
        shipment2.setId(2L);
        shipment2.setStatus(ShipmentStatus.CANCELLED);
        shipmentList.add(shipment2);

        Shipment shipment3 = new Shipment();
        shipment3.setId(3L);
        shipment3.setStatus(ShipmentStatus.IN_TRANSIT);
        shipmentList.add(shipment3);

        Shipment shipment4 = new Shipment();
        shipment4.setId(4L);
        shipment4.setStatus(ShipmentStatus.RECEIVED);
        shipmentList.add(shipment4);

        Shipment shipment5 = new Shipment();
        shipment4.setId(5L);
        shipment4.setStatus(ShipmentStatus.COMPLETED);
        shipmentList.add(shipment5);

    }


    @Test
    public void shouldGetAllCompletedShipments() throws Exception{
        String token = jwtUtils.generateJwtToken("fredrik.monsen@gmail.com");
        when(shipmentRepository.findAll()).thenReturn(new ArrayList<>());
        mvc.perform(get("/api/v1/shipments/complete")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotUpdateShipment() throws Exception {
        String token = jwtUtils.generateJwtToken("lars.olsen@gmail.com");
        Shipment updateShipment = new Shipment();
        updateShipment.setStatus(ShipmentStatus.RECEIVED);
        when(shipmentRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(shipmentList.get(0)));
        mvc.perform(put("/api/v1/shipments" + shipmentList.get(0))
                .header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateShipment)))
                .andExpect(status().isNotFound())
                .andReturn();
    }



}