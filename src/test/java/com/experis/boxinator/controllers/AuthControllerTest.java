package com.experis.boxinator.controllers;


import com.experis.boxinator.repositories.AccountRepository;
import com.experis.boxinator.models.Account;
import com.experis.boxinator.security.jwt.JwtRequest;
import com.experis.boxinator.security.jwt.JwtUtils;
import com.experis.boxinator.services.AccountService;
import com.experis.boxinator.utils.constants.AccountType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



@SpringBootTest
@AutoConfigureMockMvc
class AuthControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private JwtUtils jwtUtils;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private AccountService accountService;

    @Autowired
    private ObjectMapper objectMapper;

    private final List<Account> accountList = new ArrayList<>();


    @BeforeEach
    void setUp() {
        Account adminAccount = new Account();
        adminAccount.setId(1L);
        adminAccount.setEmail("lars.olsen@gmail.com");
        adminAccount.setRole(AccountType.ADMINISTRATOR);
        adminAccount.setActivated(true);
        accountList.add(adminAccount);

        Account userAccount = new Account();
        userAccount.setId(2L);
        userAccount.setEmail("fredrik.monsen@gmail.com");
        userAccount.setRole(AccountType.REGISTERED_USER);
        userAccount.setActivated(false);
        accountList.add(userAccount);

        Account guestAccount = new Account();
        guestAccount.setId(4L);
        guestAccount.setEmail("ali-34kongen@hotmail.com");
        guestAccount.setRole(AccountType.GUEST);
        accountList.add(guestAccount);
    }

    @Test
    public void shouldLogInGuestAccount() throws Exception{
        when(accountRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(accountList.get(2)));

        JwtRequest jwtRequest = new JwtRequest();
        jwtRequest.setEmail(accountList.get(2).getEmail());
        jwtRequest.setPassword(accountList.get(2).getPassword());

        MvcResult mvcResult = mvc.perform(post("/api/v1/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(jwtRequest)))
                .andExpect(status().isOk())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        assertTrue(content.contains("GUEST"));
    }


    @Test
    public void shouldNotLogInNotActivatedAccount() throws Exception{

    }


    @Test
    public void shouldNotLogInUnauthenticatedAccount() throws Exception{
        when(accountRepository.findById(any())).thenReturn(java.util.Optional.of(new Account()));

        JwtRequest jwtRequest = new JwtRequest();
        jwtRequest.setEmail("test@gmail.com");
        jwtRequest.setPassword("hello");

        MvcResult mvcResult = mvc.perform(post("/api/v1/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(jwtRequest)))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }
}