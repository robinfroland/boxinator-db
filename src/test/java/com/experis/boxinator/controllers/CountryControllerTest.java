package com.experis.boxinator.controllers;

import com.experis.boxinator.models.Country;
import com.experis.boxinator.repositories.CountryRepository;
import com.experis.boxinator.security.jwt.JwtUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class CountryControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private JwtUtils jwtUtils;

    @MockBean
    private CountryRepository countryRepository;

    @Autowired
    private ObjectMapper objectMapper;



    private final List<Country> countryList = new ArrayList<>();


    @BeforeEach
    void setUp() {

        Country norway = new Country();
        norway.setId(1L);
        norway.setFeeMultiplier(10.0);
        norway.setName("Norway");
        countryList.add(norway);

        Country italy = new Country();
        italy.setId(1L);
        italy.setFeeMultiplier(15.0);
        italy.setName("Italy");
        countryList.add(italy);

        Country spain = new Country();
        spain.setId(1L);
        spain.setFeeMultiplier(10.0);
        spain.setName("Spain");
        countryList.add(spain);

    }


    @Test
    public void shouldGetAllCountries() throws Exception{
        String token = jwtUtils.generateJwtToken("fredrik.monsen@gmail.com");
        when(countryRepository.findAll()).thenReturn(new ArrayList<>());
        mvc.perform(get("/api/v1/settings/countries")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());
    }


    @Test
    void shouldNotUpdateCountry() throws Exception {
        String token = jwtUtils.generateJwtToken("fredrik.monsen@gmail.com");
        Country updateCountry = new Country();
        updateCountry.setFeeMultiplier(17.2);
        when(countryRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(countryList.get(0)));
        mvc.perform(put("/api/v1/settings/countries/" + countryList.get(0))
                .header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateCountry)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }
}