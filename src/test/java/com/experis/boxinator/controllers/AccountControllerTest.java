package com.experis.boxinator.controllers;

import com.experis.boxinator.repositories.AccountRepository;
import com.experis.boxinator.models.Account;
import com.experis.boxinator.security.jwt.JwtUtils;
import com.experis.boxinator.services.AccountService;
import com.experis.boxinator.utils.constants.AccountType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private JwtUtils jwtUtils;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private AccountService accountService;

    @Autowired
    private ObjectMapper objectMapper;

    private final List<Account> accountList = new ArrayList<>();


    @BeforeEach
    void setUp() {
        Account adminAccount = new Account();
        adminAccount.setId(1L);
        adminAccount.setEmail("lars.olsen@gmail.com");
        adminAccount.setRole(AccountType.ADMINISTRATOR);
        accountList.add(adminAccount);

        Account userAccount = new Account();
        userAccount.setId(2L);
        userAccount.setEmail("fredrik.monsen@gmail.com");
        userAccount.setRole(AccountType.REGISTERED_USER);
        accountList.add(userAccount);

        Account guestAccount = new Account();
        guestAccount.setId(4L);
        guestAccount.setEmail("ali-34kongen@hotmail.com");
        guestAccount.setRole(AccountType.GUEST);
        accountList.add(guestAccount);
    }


    @Test
    public void shouldGetAuthenticatedUser() throws Exception {
        // Returns 200 if the request is  made by authenticated account
        String token = jwtUtils.generateJwtToken("fredrik.monsen@gmail.com");
        when(accountService.getAccountDetails()).thenReturn(accountList.get(1));
        mvc.perform(get("/api/v1/account")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value("fredrik.monsen@gmail.com"))
                .andReturn();
    }


    @Test
    void shouldUpdateAuthenticatedAccount() throws Exception {
        String token = jwtUtils.generateJwtToken("lars.olsen@gmail.com");
        Account updatedAccount = new Account();
        updatedAccount.setEmail( "oskarolsen@gmail.com");
        when(accountService.getAccount(any())).thenReturn(accountList.get(0));
        mvc.perform(put("/api/v1/account")
                .header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedAccount)))
                .andExpect(status().isOk())
                .andReturn();
    }
}