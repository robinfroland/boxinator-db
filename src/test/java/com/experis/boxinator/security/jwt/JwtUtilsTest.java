package com.experis.boxinator.security.jwt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@AutoConfigureMockMvc
class JwtUtilsTest {

    @MockBean
    private JwtUtils jwtUtils;

    @BeforeEach
    void setUp() {
        jwtUtils = new JwtUtils();
    }

    @Test
    public void shouldGenerateAuthToken() throws Exception {
        String token = jwtUtils.generateJwtToken("lars.olsen@gmail.com");
        assertNotNull(token);
    }

    @Test
    public void shouldValidateToken() throws Exception {
        String token = jwtUtils.generateJwtToken("lars.olsen@gmail.com");
        assertTrue(jwtUtils.validateToken(token));
        assertFalse(jwtUtils.validateToken(token.substring(0, 5)));
    }

    @Test
    public void shouldGetSameEmailFromToken() throws Exception {
        String token = jwtUtils.generateJwtToken("lars.olsen@gmail.com");
        String email = jwtUtils.getUserNameFromJwtToken(token);
        assertEquals(email, "lars.olsen@gmail.com");
        assertNotEquals(email, "fredrik.monsen@gmail.com");
    }
}